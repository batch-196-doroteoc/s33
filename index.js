//express package was imported as express
const express = require("express");
//invoked express package to create a server/api and saved it in variable which we can refer to later to create routes
const app = express();

//express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSPN from the request

//app.use() is a method used to run another function or method for our expressjs api

//It is used to run middlewares (functions that add features to our application)
app.use(express.json())
//pag wala si express json di mo marereceieve yung body


//variable for port assignment
const port = 4000;

//mock collection of courses

let courses = [

		{
			name: "Python 101",
			description: "Learn Python",
			price: 25000
		},
		{
			name: "ReactJS 101",
			description: "Learn React",
			price: 35000
		},
		{
			name: "ExpressJS 101",
			description: "Learn ExpressJS",
			price: 28000
		}
];


let users = [

	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubuTofu"
	}

];

//used the listen() method of express to assign a port to our server and send a message

//creating a route in Express:
//access express to have access to its route methods
/*
	app.method('/endpoint',(request,response)=>{
		//send() is a method similar to end() that it sends the data/message and ends the response.
		//it alos automatically creates and adds the headers

	response.send()

	})
	
*/

app.get('/',(req,res)=>{
	res.send("Hello from our first ExpressJS route!");
})

app.post('/',(req,res)=>{
	res.send("Hello from our first ExpressJS Post route!");
})

app.put('/',(req,res)=>{
	res.send("Hello from a put method route!");
})

app.delete('/',(req,res)=>{
	res.send("Hello from a delete method route!");
})

app.get('/courses',(req,res)=>{

	res.send(courses);

})

//create a route to be able to add a new course from an input from a request

app.post('/courses', (req,res)=>{

	//with express.json() the data stream has been captured, the data input has been parsed into a JS Object

	//Note: every time you need to access or use the request body, log it in the console first
	console.log(req.body)//object
	//request/req.body contains the body of the request or the input passed

	//add the input as req.body into our courses array
	courses.push(req.body)

	// console.log(courses);

	res.send(courses);

})


///////activity////////

//route - send the users array into the client

app.get('/users',(req,res)=>{
	res.send(users);
})

//route - create and add a new user obj in the array

app.post('/users', (req,res)=>{
	console.log(req.body)
	users.push(req.body)
	res.send(users);
})

//stretch goal - delete user
app.delete('/users', (req,res)=>{
	users.pop(req.body)
	// res.send(users);
	res.send("User has been deleted!");
})

// stretch goal 2 - update user
// app.put('/users',(req[],res)=>{
// 	console.log(req.body)
// 	users.splice(2,0,req.body)
// 	res.send(users); 
// })

//stillworkingonit



app.listen(port,() => console.log(`Express API running at port 4000`))